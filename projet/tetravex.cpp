#include <stdlib.h>
#include <stdio.h>
#include <fstream>
#include <iostream>
#include <stdint.h>
#include <set>
#include <vector>
#include <fstream>

using namespace std;

class Piece{
    public:
    int up;
    int right;
    int left;
    int down;
    int used;
};   


class Board{
    public:
    int ligne, col;
    Piece** tableau, **plateau;

     Board(char* filename){
        ifstream file(filename);
        file>>ligne>>col;
        plateau = new Piece*[ligne];
        tableau = new Piece*[ligne];
        for(int i=0;i< ligne;i++) {
            plateau[i] = new Piece[col];
            tableau[i] = new Piece[col];
            for(int j=0; j<col ; j++){
                file>> tableau[i][j].left >> tableau[i][j].up >> tableau[i][j].right >> tableau[i][j].down;
            }
        } 
    }

    void affiche_Tableau()
    {
        int i,j;

        for (i=0 ; i < ligne ; i++)
        {
            for (j=0 ; j<col;j++)
            {
                    cout<<"|  "<<plateau[i][j].up<<"  |";
            }cout<<endl;
            for (j=0 ; j<col;j++)
            {
                    cout<<"| "<<plateau[i][j].left<<" "<<plateau[i][j].right<<" |";
            }cout<<endl;
            for (j=0 ; j<col;j++)
            {
                    cout<<"|  "<<plateau[i][j].down<<"  |";
            }cout<<endl;cout<<endl;
            
            
        }
    }

    int colPlateau = 0, lignePlateau = 0, taille = 0;
    bool compare(int i, int j){
        
        if(lignePlateau==0 && colPlateau ==0){ 
            return true;
        }
        else if(lignePlateau==0){ 
            return (plateau[lignePlateau][colPlateau-1].right == tableau[i][j].left);
        }
        else if(colPlateau == 0){ 
            return (plateau[lignePlateau-1][colPlateau].down == tableau[i][j].up);
        }
        else{ 
            return (plateau[lignePlateau-1][colPlateau].down == tableau[i][j].up) && (plateau[lignePlateau][colPlateau-1].right == tableau[i][j].left);
        }
    }
     
    bool verifier(){
        
        for(int i=0; i<ligne; i++){

            for(int j=0;j<col;j++){
                if(!tableau[i][j].used){ 
                    if(compare(i, j)){ 
                        plateau[lignePlateau][colPlateau] = tableau[i][j];
                        tableau[i][j].used = 1;
                        
                        taille++; 
                        colPlateau = taille%col; 
                        lignePlateau = taille/ligne; 

                        if(taille == ligne*col || verifier()) 
                            return true;
                        else{
                            tableau[i][j].used = 0; 
                            taille--; 
                            colPlateau = taille%col; 
                            lignePlateau = taille/ligne; 
                        }
                    }
                }
            }
        }
        return false; 
    }


   
};


int main(int argc,char**argv)
{   
    Board b(argv[1]);
    const clock_t begin_time = clock();
    if(b.verifier()){
        float duration = float( clock () - begin_time ) /  CLOCKS_PER_SEC;
        b.affiche_Tableau();
        cout<<"Resolu en : " << duration<<"s"<<endl;
    }    
    else
        cout<<"Aucune solution"<<endl;
   return 0;
}

